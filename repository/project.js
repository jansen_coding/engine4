const knex = require('../config/dbConnection');
const _ = require('lodash');
 
// Gets one proyect
async function fetchOne(params){
  let projects = await fetch(params);
  return projects[0] || null;
}

//Gets all the projects with respective data
async function fetch(params){
  try{
    let query = knex('project');
    query.offset(params.offset);
    query.limit(params.limit);

    if (params.filter.keywords) {
      query.andWhere('name', '=', params.filter.keywords);
    }
    if (params.filter.tag) {
      query.andWhere('tag', '=', params.filter.tag);
    }
    if (params.filter.sort) {
      query.orderBy(params.filter.sort, 'desc');
    }
    if (params.filter.id) {
      query.where('id', params.filter.id);
    }
    //console.log(query.toSQL().toNative());
   
    let projects = await query;
    let idList = projects.map(a => a.id);

    let imageRows  =  await knex.select().from('project-photo').whereIn('id_project',idList);
    let progressRows = await knex('progress').whereIn('id_project',idList);
    let members = await knex('account').join('project-member', 'account.id', 'project-member.id_account')
      .join('member','member.id_member', 'project-member.id')
      .select('account.name','account.last', 'account.email','project-member.*', 'member.id_project');

    let partners = await knex('partner').join('project-partner', 'partner.id', 'project-partner.id_partner')
      .join('project','project.id', 'project-partner.id_project')
      .select('partner.*','project-partner.id_project');

    for(let i = 0; i < projects.length; i++) {
      projects[i].images = _.filter(imageRows , {id_project: projects[i].id});
      projects[i].members = _.filter(members , {id_project: projects[i].id});
      projects[i].progress = _.filter(progressRows , {id_project: projects[i].id});
      projects[i].progress = projects[i].progress.sort(function(a, b){return a.created_in - b.created_in})
      
      projects[i].partners = _.filter(partners , {id_project: projects[i].id});
    }
    return  projects;
  }
   catch (e){
    console.error(e);
    throw  ('Error fetching projects');
    };
   
};

// Inserts a project in project table.
async function insertOne(params){
  return await knex('project').returning(['id', 'created_in'])
  .insert({name: params.name, description: params.description, 
    git: params.git, state: params.state, external_url: params.url,
     main_pic: params.main_pic,
    banner_pic: params.banner_pic, tag: params.tag});
}
// Delete a project
async function deleteRow(id){
  await knex('project').where('id','=',id).del();
}
// Update a project
async function updtRow(params){
  await knex('project').where('id','=', params.id)
    .update({name: params.name, description: params.description, 
    git: params.git, state: params.state, external_url: params.external_url,
     main_pic: params.main_pic,
    banner_pic: params.banner_pic, tag: params.tag, is_top: params.is_top});
}

//Insert Project Photo to table
async function insertPhoto(params){
   return await knex('project-photo').returning(['id', 'created_in'])
    .insert({name: params.name, id_project: params.id_project})
}
//Delete Project Photo from table
async function deletePhoto(id){
  await knex('project-photo').where('id','=', id).del()
}
//Insert Project Progress to table
async function insertProgress(params){
  return await knex('progress').returning(['id', 'created_in'])
    .insert({summary: params.summary, pdf_atached: params.pdf_atached,
    id_project: params.id_project})
    
}
//Delete Project Progress from table
async function deleteProgress(id){
await knex('progress').where('id','=', id).del()
}
//Insert Project Members to table
async function insertMembers(params){
  await knex('member').insert({id_project: params.id_project, id_member: params.id_member })
}
//Delete Project Members to table
async function deleteMembers(params){
  await knex('member').where('id_project','=', params.id).andWhere('id_member', '=', params.id_member).del() ;
}

//Insert Project Partner to table
async function insertPartner(params){
  await knex('project-partner').insert({id_project: params.id_project, id_partner: params.id_partner })
}
//Delete Project Partner to table
async function deletePartner(params){
  await knex('project-partner').where('id_partner', '=', params.id_partner).andWhere('id_project','=', params.id).del() ;
}

module.exports = {
  fetchOne,
  fetch,
  insertOne,
  deleteRow,
  updtRow,
  insertPhoto,
  deletePhoto,
  insertProgress,
  deleteProgress,
  insertMembers,
  deleteMembers,
  insertPartner,
  deletePartner
};
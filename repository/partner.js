const knex = require('../config/dbConnection');


function fetch(){
    let sponsors = knex('partner');
    
    return sponsors;
}

function fetchOne(id){
    let sponsors = knex('partner').where('id' , id);
    
    return sponsors;
} 

async function insertOne(params){
    return await knex('partner').returning(['id', 'created_in']).insert({name: params.name, url: params.url, 
        logo: params.logo});
    }

async function deleteRow(id){
    await knex('partner').where('id','=',id).del();
}

async function updtRow(params){
    await knex('partner').where('id','=', params.id).update({name: params.name, url: params.url, 
        logo: params.logo});
}

module.exports = {
    fetch, 
    fetchOne,
    insertOne,
    deleteRow,
    updtRow
}
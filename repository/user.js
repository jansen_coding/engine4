const knex = require('../config/dbConnection');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const saltRounds = 10;

// Gets one user
async function fetchOne(params){
    let projects = await fetch(params);
    return projects[0] || null;
  }

// Gets all users  
async function fetch(params){
    try{
      let query = knex('account').leftOuterJoin('rol','account.id_rol', 'rol.id')
        .leftJoin('project-member', 'account.id', 'project-member.id_account');
      
      query.offset(params.offset);
      query.limit(params.limit);
  
      if (params.filter.keywords) {
        query.andWhere('name', '=', params.filter.keywords);
      }
      
      if (params.filter.sort) {
        query.orderBy(params.filter.sort, 'desc');
      }
      if (params.filter.id) {
        query.where('account.id', params.filter.id);
      }
     
      
      let members = await query.select('rol.title','account.name','account.last',
         'account.id_rol', 'account.email', 'project-member.*');
         
      let idList = members.map(a => a.id);
  
      let imageRows  =  await knex.select('name', 'created_in','id_member').from('member-photo')
        .whereIn('id_member',idList);

      let subquery = knex('member').select('id_project').whereIn('id_member',idList);

      let projects = await knex('project').rightJoin('member', 'project.id', 'member.id_project')
      .select('member.id_member', 'project.id', 'project.name').whereIn('project.id',subquery);

      for(let i = 0; i < members.length; i++) {
        members[i].images = _.filter(imageRows , {id_member: members[i].id});
        members[i].enrolled_projects = _.filter(projects , {id_member: members[i].id});
      }
      return  members;
    }
     catch (e){
      console.error(e);
      throw  ('Error fetching users');
      };
  };
  
async function isMember(id_account, id_project){
    let member = await knex('project-member').select('id').where('id_account', '=', id_account)
    
    let is_member = await knex('member').where('id_member' , member[0].id).andWhere('id_project', id_project)
    if (is_member.length > 0)
        return true;

    return false;
} 
// Inserts a user in account and proyect-member table.
async function insertOne(params){
    bcrypt.hash(params.password, saltRounds, async function(err, hash) {
        if(err){
            console.log(err);
            throw Error(err.message);
        }else{

            const id = await knex('account').insert({password: hash, name: params.name, last: params.last,
            email: params.email, id_rol: params.id_rol})
            .returning('id')
            await knex('project-member').insert({description: params.description, age: params.age, git: params.git,
                 url: params.url, twitter: params.twitter, id_account: id[0], profile_pic: params.profile_pic})
        }
    });
  }

  // Update a user
async function updtRow(params){
      bcrypt.hash(params.password, saltRounds, async function(err, hash) {
        if(err){
            console.log(err);
            throw Error(err.message);
        }else{
            await knex('account').where('id','=', params.id).update({password: hash, name: params.name, 
                last: params.last, email: params.email, id_rol: params.id_rol} );
    
            await knex('project-member').where('id_account','=', params.id).update({
                description: params.description, age: params.age, git: params.git, url: params.url, 
                twitter: params.twitter, modifyed_in: 'today', id_account: params.id, profile_pic: params.profile_pic})
        }
    });
  }
  // Update a user
  async function deleteRow(id){
    await knex('account').where('id','=',id).del();
    await knex('project-member').where('id_account','=',id).del();
  }
  //Inser User Photo to table
  async function insertPhoto(params){
      let subquery = knex('project-member').where('id_account','=', params.id).select('id');
      await knex('member-photo').insert({name: params.name, id_member: subquery})
  }
  //Delete User Photo from table
  async function deletePhoto(id){
    await knex('member-photo').where('id','=', id).del()
}
module.exports = {
    isMember,
    fetch,
    fetchOne,
    insertOne,
    updtRow,
    deleteRow,
    insertPhoto,
    deletePhoto
}
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const subdomain = require('express-subdomain');
require('./config/strategies/local.strategy')();

const adminRouter = require('./src/routes/adminRoutes')();
const app = express();

app.use(cookieParser());
app.use(passport.initialize());

//require('./config/passport.js')(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(subdomain('admin', adminRouter));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/resource', express.static(__dirname + '/resource'));
app.use('/css', express.static(path.join(__dirname,'/node_modules/bootstrap/dist/css')))
app.use('/js', express.static(path.join(__dirname,'/node_modules/bootstrap/dist/js')))
app.use('/js', express.static(path.join(__dirname,'/node_modules/jquery/dist')))

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

const mainRouter = require('./src/routes/mainRoutes')();
const projectRouter = require('./src/routes/projectRoutes')();

app.use('/', mainRouter)
app.use('/project', projectRouter);

require('./config/server')(app);

const knex = require('../dbConnection');
const passport = require('passport');
const passportJWT = require("passport-jwt");
  
module.exports = function localStrategy(){

    let ExtractJwt = passportJWT.ExtractJwt;
    let JwtStrategy = passportJWT.Strategy;

    let jwtOptions = {}
    jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    jwtOptions.secretOrKey = 'tasmanianDevil';

    var strategy = new JwtStrategy(jwtOptions, async function(jwt_payload, next) {

        try{
            console.log('payload received', jwt_payload);

            let user = await knex('account').select('id','email').where('id', jwt_payload.id);
            console.log(user[0])
            if (user.length==0) {
                return next(null, false, { error:{message: 'Token Error.' }});
            }
            else {
                return next(null, user[0]);
            }   
        }
        catch (e){
            return next(e);
        }
    });

    passport.use(strategy);
}







const port = process.env.PORT || 3000;

module.exports = function serverConnection(app){
    app.listen(port, function(){
    console.log('listening on '+port);
    });
};
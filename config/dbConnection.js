require('dotenv').config();
var knex = require('knex')({
    client: 'pg',
    connection: {
      host : process.env.HOST,
      user : process.env.USER,
      password : process.env.PASSWORD,
      database : process.env.DB_NAME
    },
    pool: { min: 0, max: 10 }
  });

  module.exports = knex;
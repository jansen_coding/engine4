const passport = require('passport');
const knex = require('../config/dbConnection');
require('./strategies/local.strategy')();

module.exports= function(app){
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(async function(user, done) {
    try{
    let data = await knex('account').join('project-member','account.id', 'project-member.id_account' )
    .select('account.id', 'account.email', 'account.name', 'account.last', 'account.id_rol', 
    'project-member.profile_pic').where('account.email', user.email);
  
    done(null, data[0]);
    }
    catch(e){
        console.log('Error when selecting user on session deserialize' + e)
        return done(e)};
});

}

const express = require('express');
const adminRouter = express.Router();
const projectRepo = require('../../repository/project');
const knex = require('../../config/dbConnection');
const userRepo = require('../../repository/user');
const partnerRepo = require('../../repository/partner');
const passport = require('passport');
const path = require('path');
const validator = require('validator');
const cors = require('cors');
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
const multer = require('multer');

var passportJWT = require("passport-jwt");
var ExtractJwt = passportJWT.ExtractJwt;

var jwtOptions = {}
    jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    jwtOptions.secretOrKey = 'tasmanianDevil';

const DIR = './resource';
 
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        console.log(file)
        cb(null, DIR);
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    }
    });


function validatePartner(req){
    if(validator.isEmpty(req.body.name)) 
        throw Error('Invalid name'); 
    if(!validator.isURL(req.body.url))
        throw Error('Invalid url');
    if(validator.isEmpty(req.body.logo))
        throw Error('Invalid logo');
}

function validateProject(req){
    if(validator.isEmpty(req.body.name)) 
        throw Error('Invalid name');
    if(req.body.git !== null && req.body.git !== undefined )
       if(!validator.isURL(req.body.git))
         throw Error('Invalid url'); 
    if(req.body.url !== null && req.body.url !== undefined)
        if(!validator.isURL(req.body.url))
          throw Error('Invalid url');
}

function validateUser(req){
    if(validator.isEmpty(req.body.password) || !(validator.isLength(req.body.password,4,25))) 
        throw Error('Invalid password. Must be at least 4 characters.');
    if(validator.isEmpty(req.body.name))
        throw Error('Invalid name.');
    if(validator.isEmpty(req.body.last))
        throw Error('Invalid last name.');
    if(!(validator.isEmail(req.body.email)))
        throw Error('Invalid e-mail.');
    if(new Number(req.body.id_rol) < 1 || new Number(req.body.id_rol) > 4)
        throw Error('Invalid rol_id.');
    if(req.body.git !== null)
        if(!validator.isURL(req.body.git))
            throw Error('Invalid git url'); 
    if(req.body.url !== null)
        if(!validator.isURL(req.body.url))
            throw Error('Invalid url');
    if(req.body.twitter !== null)
        if(!validator.isURL(req.body.twitter))
            throw Error('Invalid twitter url');
};

function router(){
    adminRouter.use(cors());
    adminRouter.route('/partners') 
    .get( passport.authenticate('jwt', { session: false }), async function(req, res)  {
        try {
            const partners = await partnerRepo.fetch() ;
            res.json(partners);
        }
        catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
    })

    .post( passport.authenticate('jwt', { session: false }), async function(req, res)  {
        try {
            validatePartner(req);
            let partner = req.body;
            let id = await partnerRepo.insertOne(partner);
            partner.id = id[0].id;
            partner.created_in = id[0]. created_in
            res.json(partner);
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      });

    adminRouter.route('/partners/:id')
    .get(passport.authenticate('jwt', { session: false }), async function (req, res) {
        try{
            const {id} = req.params;
            const partner = await partnerRepo.fetchOne(id);
            res.json({
                partner
            });
        }
        catch(e){
            console.error(e);
            res.json({
            error: e.message
            });
        }
    })
    .put(passport.authenticate('jwt', { session: false }), async function (req, res) {
        try{
            validatePartner(req)
            
            req.body.id = req.params.id;
            let params = req.body
            await partnerRepo.updtRow(params);
            res.json('Partner Updated');
        }
        catch(e){
            console.error(e);
            res.json({
            error: e.message
            });
        }
    })
    .delete(passport.authenticate('jwt', { session: false }), async function(req, res)  {
        try {
            const {id} = req.params;
            await partnerRepo.deleteRow(id);
            res.json('Partner Deleted');
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
    });

    adminRouter.route('/')
    .get((req,res) => {
        res.redirect('/login');
    })
    adminRouter.route('/login')
    .post(async function(req, res) {
        try {
            if(req.body.name && req.body.password){
                let {name} = req.body;
                let {password} = req.body;
                
                console.log(req.body)

                let users = await knex('account').leftJoin('project-member','account.id', 'project-member.id_account' )
                   .select('account.id as id','account.password', 'account.email', 'account.name', 'account.last', 'account.id_rol', 
                   'project-member.id as id_member', 'project-member.profile_pic').where('account.email', '=', name); 
                if(users.length===0 || users.length===undefined ){
                res.status(401).json({error:{message:"no such user found"}});
                }
                else{
            
                    bcrypt.compare(password, users[0].password, function(err, resp) {
                        if (resp) {
                            let payload = {id: users[0].id};
                                    console.log(payload)
                            let token = jwt.sign(payload, jwtOptions.secretOrKey, { expiresIn: 60 * 60 });
                            console.log(users)
                            res.json({ id:users[0].id,
                                id_member: users[0].id_member,
                                email: users[0].email,
                                name: users[0].name,
                                last: users[0].last,
                                id_rol: users[0].id_rol,
                                profile_pic: users[0].profile_pic,
                                token: token});
                            }
                        else{ 
                            res.status(401).json({error:{message:"passwords did not match"}});
                        }
                    }); 
                    
                }  
            }
        }
        catch(e){
            console.error(e);
            res.json({
            error: e.message
            });
        }  
      });

    adminRouter.route('/projects')
    .get(passport.authenticate('jwt', { session: false }),async function (req,res) {
        try {
            let params = {
                offset : parseInt(req.params.offset || 0),
                limit:  parseInt(req.params.limit || 25),
                filter: { id: req.query.id || null, 
                          keyword: req.query.keyword || null, 
                          tag:req.query.tag || null,
                          sort:req.query.sort || null}
              };
        const projects = await projectRepo.fetch(params);
        res.json(
            projects
        );
        }
        catch(e){
            console.error(e);
            res.json({
            error: e.message
            });
        }
    })
    .post(passport.authenticate('jwt', { session: false }),function (req,res) {
        let upload = multer({storage: storage}).single('project_photo');
        try {
            
            upload(req, res, async function(err) {
                if (err) {
                    console.log("No file received");
                    return res.json({
                        success: false
                    });
                }
                if(!req.file){
                validateProject(req);
                let project = req.body;
                console.log(req.body)
                let id = await projectRepo.insertOne(project);
                //console.log(id)
                project.id = id[0].id;
                project.created_in = id[0]. created_in
                res.json(project)
                }else{
                    let path ='';
                    //const host = req.hostname;
                    //const filePath = req.protocol + "://" + host + '/' + req.file.path;
                    //console.log(filePath);
                    //console.log(req.file)
                    console.log('file received');
                    path = req.file.path;
                    return res.send("Upload Completed for "+path); 
                }
            });
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
    adminRouter.route('/projects/:id')
    .get(passport.authenticate('jwt', { session: false }), async function (req,res) {
     try{
        let params = {
            offset : parseInt(req.params.offset || 0),
            limit:  parseInt(req.params.limit || 25),
            filter: { id: req.params.id || null, 
            keyword: req.query.keyword || null, 
            tag:req.query.tag || null,
            sort:req.query.sort || null},
            };
            let project = await projectRepo.fetchOne(params);
            res.json({
            project
        });
      }catch(e){
        console.error(e);
        res.json({
        error: e.message
        });
      }   
    })
    .delete(passport.authenticate('jwt', { session: false }),async function (req,res) {
        try {
            if(true){
                const {id} = req.params;
                await projectRepo.deleteRow(id);
                res.json('Project Deleted');
            }else{
                //res.send('No Permit For That Action');
            }
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
    .put(passport.authenticate('jwt', { session: false }),function (req,res) {
        let upload = multer({storage: storage}).single('project_photo');
        try {
            upload(req, res, async function(err) {
            if (err) {
                console.log("No file received");
                return res.json({
                    success: false
                });
            }
            console.log(req.file)
            if(!req.file){
                validateProject(req);
                let project = req.body;
                console.log(req.body)
                await projectRepo.updtRow(project);
                console.log(project.id)
                res.json('Project Updated');
                }else{
                    let path ='';
                    //const host = req.hostname;
                    //const filePath = req.protocol + "://" + host + '/' + req.file.path;
                    //console.log(filePath);
                    //console.log(req.file)
                    console.log('file received');
                    path = req.file.path;
                    return res.json("Upload Completed for "+path); 
                }
            });
            //if(req.user.id_rol === 1 || await userRepo.isMember(req.user.id,req.params.id)){
             

        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      adminRouter.route('/projects/:id/photos')
      .post(passport.authenticate('jwt', { session: false }), async function (req,res) {
        try {
            let image = req.body;
            let id = await projectRepo.insertPhoto(image);
            image.id = id[0].id;
            image.created_in = id[0]. created_in
            res.json({image})
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      adminRouter.route('/projects/:id/photos/:pic')
      .delete(passport.authenticate('jwt', { session: false }), async function (req,res) {
        try {
                const {id} = req.params;
                const {pic} = req.params;
                console.log(id, pic);
                await projectRepo.deletePhoto(pic);
                res.json('Project Picture Deleted');
            //}else {
                //res.send('No Permit For That Action');
           // }
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      adminRouter.route('/projects/:id/progress')
      .post(passport.authenticate('jwt', { session: false }), async function (req,res) {
        try {
            let progress = req.body;
            let id = await projectRepo.insertProgress(progress);
            progress.id = id[0].id;
            progress.created_in = id[0]. created_in
            res.json({progress})
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      adminRouter.route('/projects/:id/progress/:prog')
      .delete(passport.authenticate('jwt', { session: false }), async function (req,res) {
        try {
            //if(req.user.id_rol === 1){
                const {id} = req.params;
                const {prog} = req.params;
                console.log(id, prog);
                await projectRepo.deleteProgress(prog);
                res.json('Project Progress Deleted');
            //}else {
                //res.send('No Permit For That Action');
           // }
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      adminRouter.route('/projects/:id/addmembers')
      .post(passport.authenticate('jwt', { session: false }), async function (req,res) {
        try {

            let params = req.body;
            console.log(req.body)
            await projectRepo.insertMembers(params);
            res.json('Project Member Added');
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      adminRouter.route('/projects/:id/addmembers/:id_member')
      .delete(passport.authenticate('jwt', { session: false }), async function (req,res) {
        try {
            //if(req.user.id_rol === 1){
                const {id} = req.params;
                const {id_member} = req.params;
                let params = {id, id_member};
                console.log(params);
                await projectRepo.deleteMembers(params);
                res.json('Project Member Deleted');
            //}else {
                //res.send('No Permit For That Action');
           // }
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      adminRouter.route('/projects/:id/addpartner')
      .post(passport.authenticate('jwt', { session: false }), async function (req,res) {
        try {
            req.body.id = req.params.id;
            let params = req.body;
            await projectRepo.insertPartner(params);
            res.json('Pproject Partner Added');
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      adminRouter.route('/projects/:id/addpartner/:id_partner')
      .delete(passport.authenticate('jwt', { session: false }), async function (req,res) {
        try {
            //if(req.user.id_rol === 1){
                const {id} = req.params;
                const {id_partner} = req.params;
                let params = {id, id_partner};
                console.log(params);
                await projectRepo.deletePartner(params);
                res.json('Project Partner Deleted');
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      adminRouter.route('/users')
      .get(passport.authenticate('jwt', { session: false }),async function (req,res) {
        try {
            let params = {
                offset : parseInt(req.params.offset || 0),
                limit:  parseInt(req.params.limit || 25),
                filter: { id: req.query.id || null, 
                          keyword: req.query.keyword || null, 
                          tag:req.query.tag || null, 
                          sort:req.query.sort || null}
              };
        const users = await userRepo.fetch(params);
        res.json(users);
        }
        catch(e){
            console.error(e);
            res.json({
            error: e.message
            });
        }
    })
    .post(passport.authenticate('jwt', { session: false }), async function (req,res) {
        try {
            //if(req.user.id_rol === 1){
                validateUser(req);
                let params = req.body;
                await userRepo.insertOne(params);
                res.send('User Added');
           // }else {
            //    res.send('No Permit For That Action');
            //}
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
    adminRouter.route('/users/:id')
    /*.all((req, res, next) => {
        if(req.user){
            console.log(req.user);
            next();
        } else {
            res.redirect('/');
        }
    })*/
    .get(async function (req,res) {
        try{
           let params = {
               offset : parseInt(req.params.offset || 0),
               limit:  parseInt(req.params.limit || 25),
               filter: { id: req.params.id || null, 
               keyword: req.query.keyword || null, 
               tag:req.query.tag || null,
               sort:req.query.sort || null},
               };
               let user = await userRepo.fetchOne(params);
               res.json({
               data: user
           });
         }catch(e){
           console.error(e);
           res.json({
           error: e.message
           });
         }   
       })
       .put(async function (req,res) {
        try {   
            if(req.user.id_rol === 1 || req.user.id === req.params.id){
                validateUser(req);
                req.body.id = req.params.id;
                let params = req.body;
                await userRepo.updtRow(params);
                res.send('User Updated');
            }else{
                res.send('No Permit For That Action');
            }

        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      .delete(async function (req,res) {
        try {
            if(req.user.id_rol === 1){
                const {id} = req.params;
                await userRepo.deleteRow(id);
                res.send('User Deleted');
            }else{
               res.send('No Permit For That Action');
            }
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      adminRouter.route('/users/:id/photos')
      .post(async function (req,res) {
        try {
            //if(req.user.id_rol === 1){
                req.body.id = req.params.id;
                let params = req.body;
                await userRepo.insertPhoto(params);
                res.send('User Picture Added');
            //}else {
             //   res.send('No Permit For That Action');
            //}
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
      adminRouter.route('/users/:id/photos/:pic')
      .delete(async function (req,res) {
        try {
            //if(req.user.id_rol === 1){
                const {id} = req.params;
                const {pic} = req.params;
                console.log(id, pic);
                await userRepo.deletePhoto(pic);
                res.send('User Picture Deleted');
            //}else {
                //res.send('No Permit For That Action');
           // }
        }catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })


    return adminRouter;
}
module.exports = router;


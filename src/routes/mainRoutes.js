const express = require('express');
const mainRouter = express.Router();
const projectRepo = require('../../repository/project');
const partnerRepo = require('../../repository/partner');
const rssService = require('../../services/rssService');
const eventService = require('../../services/eventService');
const instaService = require('../../services/instagramService');
const path = require('path');

function router(){
    mainRouter.get('/', async function(req, res)  {
        try {
            let params = {
            offset : parseInt(req.params.offset || 0),
            limit:  parseInt(req.params.limit || 25),
            filter: { id: req.query.id || null, 
            keyword: req.query.keyword || null, 
            tag:req.query.tag || null,
            sort:req.query.sort || null},
            };
    
            //Get projects
        let projects = await projectRepo.fetch(params);
            projects = projects.sort(function(a, b){return a.created_in - b.created_in})
            // Get sponsors
            let sponsors = await partnerRepo.fetch();
            
            // Get IOT Rss
            const iotNews = await rssService.serve('iot');
            // Get Artificial Intelligence Rss
            const aiNews = await rssService.serve('ai');
            // Get Robotics Rss
            const robNews = await rssService.serve('rob');
            // Get Big Data Rss
            const bdNews = await rssService.serve('bd');
            // Get 3D Printing Rss
            const printingNews = await rssService.serve('3d');
            // Get Machine Learning Rss
            const machineNews = await rssService.serve('ml');
            // Get Information Security Rss
            //const securityeNews = await rssService.serve('if');
            // Get Smart Cities Rss
            //const securityeNews = await rssService.serve('sc');
            
            const events =await eventService.events();
            const insta = await instaService.feed();
            res.render(path.join(__dirname, '../../views/index-default-particles'),{
            
            projects,
            sponsors,
            events,
            iotNews,
            aiNews,
            robNews,
            bdNews,
            printingNews,
            machineNews,
            insta
            
        });
        } catch(e) {
            console.error(e);
            res.json({
            error: e.message
            });
        }
    });
    return mainRouter;
}
module.exports = router;
const express = require('express');
const projectRouter = express.Router();
const projectRepo = require('../../repository/project');
const path = require('path');

function router(){
  projectRouter.route('/')
    .get(async function (req,res) {
        try {
            let params = {
                offset : parseInt(req.params.offset || 0),
                limit:  parseInt(req.params.limit || 25),
                filter: { id: req.query.id || null, 
                    keyword: req.query.keyword || null, 
                    tag:req.query.tag || null,
                    sort:req.query.sort || null}
              };
        let projects = await projectRepo.fetch(params);
        projects = projects.sort(function(a, b){return a.created_in - b.created_in})
        res.render(path.join(__dirname, '../../views/index-projects'),{
            projects
        });
        }
        catch(e){
            console.error(e);
            res.json({
            error: e.message
            });
        }
    });

  projectRouter.route('/:id')
 /*   .all(async function (req,res,next) {
        try{
        
            next();
        }
        catch(e){
            console.error(e);
            res.json({
            error: e.message
            });
        }
      })
*/
    .get(async function (req,res) {
        try{
            let params = {
            offset : parseInt(req.params.offset || 0),
            limit:  parseInt(req.params.limit || 25),
            filter: { id: req.params.id || null, 
            keyword: req.query.keyword || null, 
            tag:req.query.tag || null,
            sort:req.query.sort || null},
            };
            let projects = await projectRepo.fetchOne(params);
            
            res.render(path.join(__dirname, '../../views/index-project-single'),{
            projects
        });
    }catch(e){
        console.error(e);
        res.json({
        error: e.message
        });
    }
  })
return projectRouter;
}

module.exports = router;
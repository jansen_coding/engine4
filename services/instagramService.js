const redis = require('redis');
let client = redis.createClient();
const axios = require('axios');
const {promisify} = require('util');
const getAsync = promisify(client.get).bind(client);

function instaService(){
    async function feed(){
        try{
            const res = await getAsync('insta');
            if (res !== null){
                return JSON.parse(res);

            }else{
                let data = await new Promise((resolve, reject) => {
                axios.get('https://api.instagram.com/oembed/?url=https://www.instagram.com/p/BoutE8YHVHc/?taken-by=google')
                    .then((response) => {
                        let data = response.data
                        resolve ({ title:data.title,
                                    author: data.author_name,
                                    author_url: data.author_url,
                                    html: data.html
                            });
                    })
                    .catch((error) => {
                        reject(error);
                        console.log(error);
                    });
                });
            client.SETEX('insta', 43200, JSON.stringify(data));
            return data;
            }
        } catch(e){
            console.error(e);
            throw  ('Error fetching instagram cache');
        }
    }
    return {feed};
};

module.exports = instaService();
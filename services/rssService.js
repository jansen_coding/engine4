const axios = require('axios');
const xml2js  = require('xml2js');
const redis = require('redis');
let client = redis.createClient();
const {promisify} = require('util');
const getAsync = promisify(client.get).bind(client);

const parser = xml2js.Parser({ explicitArray: false });

function rssService(){
    async function serve(whereToGet){
        try{
            const res = await getAsync(whereToGet);
            if (res !== null){
                return JSON.parse(res);
            }else{

                let data =  await new Promise((resolve, reject) => {
                    let link;
                    switch(whereToGet){
                        case 'iot':  link = 'https://flipboard.com/topic/internetofthings/rss';
                        break;
                        case 'ai': link = 'https://flipboard.com/topic/artificialintelligence/rss';
                        break;
                        case 'rob': link = 'https://flipboard.com/topic/robotics/rss';
                        break;
                        case 'bd': link = 'https://flipboard.com/topic/bigdata/rss';
                        break;
                        case '3d': link = 'https://flipboard.com/topic/3dprinting/rss';
                        break;
                        case 'ml': link = 'https://flipboard.com/topic/machinelearning/rss';
                        break;
                        //case 'is': link = 'https://flipboard.com/topic/informationsecurity/rss';
                    // break;
                    //case 'sc': link = 'https://flipboard.com/topic/smartcities/rss';
                    //break;
                    }
                    axios.get(link)
                        .then((response) => {
                            parser.parseString(response.data, (err, result) =>{
                                if(err){
                                    console.log(err);
                                }else{
                                    resolve({
                                        title: result.rss.channel.title,
                                        link: result.rss.channel.link,
                                        news: result.rss.channel.item.slice(0,6)
                                    });
                                }
                            });
                        })
                        .catch((error) => {
                            reject(error);
                            console.log(error);
                        });
                });
                client.SETEX(whereToGet, 14400, JSON.stringify(data));
                return data;
            }
        } catch(e){
            console.error(e);
            throw  ('Error fetching news cache');
        }
        
    }
    return {serve};
};

module.exports = rssService();
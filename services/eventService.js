const redis = require('redis');
let client = redis.createClient();
const axios = require('axios');
const {promisify} = require('util');
const getAsync = promisify(client.get).bind(client);

function eventService(){
    async function events(){
        try{
            const res = await getAsync('ev');
            if (res !== null){
                return JSON.parse(res);

            }else{
                let data = await new Promise((resolve, reject) => {
                axios.get('https://www.eventbriteapi.com/v3/events/search/?organizer.id=14966585836&token=JFDIIDH6D7ICM6QTZZ6R')
                    .then((response) => {
                        let data = response.data
                        resolve ({ events:data.events
                            });
                    })
                    .catch((error) => {
                        reject(error);
                        console.log(error);
                    });
                });
            client.SETEX('ev', 43200, JSON.stringify(data));
            return data;
            }
        } catch(e){
            console.error(e);
            throw  ('Error fetching events cache');
        }
    }
    return {events};
};

module.exports = eventService();